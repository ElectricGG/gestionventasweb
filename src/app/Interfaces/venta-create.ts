import { VentaDetalleCreate } from "./venta-detalle-create";

export interface VentaCreate {
    id: number;
    clienteId: number;
    asesorId: number;
    fechaVenta: Date;
    ventaDetalles: VentaDetalleCreate[];
}
