import { Producto } from "./producto";

export interface VentaDetalles {
    ventaId: number;
    productoId: number;
    producto: Producto;
    precioUnitario: number;
    cantidad: number;
    subtotal:number
}
