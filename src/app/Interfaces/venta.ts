import { VentaDetalles } from "./venta-detalles";
import { Asesor } from "./asesor";
import { Cliente } from "./cliente";

export interface Venta {
    id: number;
    clienteId: number;
    cliente: Cliente;
    asesorId: number;
    asesor: Asesor;
    fechaVenta: Date;
    ventaDetalles: VentaDetalles[];
    totalventa:number;
}
