export interface VentaDetalleCreate {
    productoId: number;
    precioUnitario: number;
    cantidad: number;
}
