import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Asesor } from '../Interfaces/asesor';
@Injectable({
  providedIn: 'root'
})
export class AsesorService {
  private endpoint:string = environment.endPoint;
  private apiUrl:string = this.endpoint;
  private token:string = environment.token;
  private headers: HttpHeaders = new HttpHeaders().set('Authorization', `Bearer ${this.token}`);

  constructor(private http:HttpClient) { }

  

  getListAsesor():Observable<Asesor[]>{
    return this.http.get<Asesor[]>(`${this.apiUrl}GetAsesores/lista`,{ headers: this.headers });
  }
}
