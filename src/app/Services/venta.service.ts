import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http'
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Venta } from '../Interfaces/venta';
import { VentaCreate } from '../Interfaces/venta-create';

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  private endpoint:string = environment.endPoint;
  private apiUrl:string = this.endpoint;
  private token:string = environment.token;

  private headers: HttpHeaders = new HttpHeaders().set('Authorization', `Bearer ${this.token}`);

  constructor(private http:HttpClient) { }

  createVenta(modelo:VentaCreate):Observable<VentaCreate>{
    return this.http.post<VentaCreate>(`${this.apiUrl}CreateVenta/create-venta`,modelo,{ headers: this.headers });
  }
  
  getListVenta():Observable<Venta[]>{
    return this.http.get<Venta[]>(`${this.apiUrl}GetVentas/lista`,{ headers: this.headers });
  }

  updateVenta(modelo:Venta):Observable<Venta>{
    return this.http.put<Venta>(`${this.apiUrl}UpdateVenta/update-venta`,modelo,{ headers: this.headers });
  }

  deleteVenta(id:number):Observable<string>{
    return this.http.delete<string>(`${this.apiUrl}DeleteVenta/delete-venta?id=`+id,{ headers: this.headers });
  }
}
