import { Injectable } from '@angular/core';
import { Producto } from '../Interfaces/producto';
import { environment } from 'src/environments/environment';
import { HttpClient,HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  private endpoint:string = environment.endPoint;
  private apiUrl:string = this.endpoint;
  private token:string = environment.token;
  private headers: HttpHeaders = new HttpHeaders().set('Authorization', `Bearer ${this.token}`);

  constructor(private http:HttpClient) { }

  getListProducto():Observable<Producto[]>{
    return this.http.get<Producto[]>(`${this.apiUrl}GetProductos/lista`,{ headers: this.headers });
  }
}
