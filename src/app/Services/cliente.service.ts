import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient,HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Cliente } from '../Interfaces/cliente';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private endpoint:string = environment.endPoint;
  private apiUrl:string = this.endpoint;
  private token:string = environment.token;
  private headers: HttpHeaders = new HttpHeaders().set('Authorization', `Bearer ${this.token}`);
  constructor(private http:HttpClient) { }

  getListCliente():Observable<Cliente[]>{
    return this.http.get<Cliente[]>(`${this.apiUrl}GetClientes/lista`,{ headers: this.headers });
  }
}
