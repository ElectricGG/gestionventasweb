import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//Para trabajar con reactive forms
import {ReactiveFormsModule} from '@angular/forms';

//para trabajar con peticiones http
import { HttpClientModule } from '@angular/common/http';

//para teabajar con tablas de material
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';

//para trabajar con controles de formularios de material
import {MatFormFieldModule} from '@angular/material/form-field'
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import { MomentDateModule } from '@angular/material-moment-adapter';

//para trabajar con alertas
import {MatSnackBarModule} from '@angular/material/snack-bar';

//para trabajar con iconos de material
import {MatIconModule} from '@angular/material/icon';

//para trabajar con modales de material
import {MatDialogModule} from '@angular/material/dialog';

//para trabajar con cuadriculas
import {MatGridListModule} from '@angular/material/grid-list';
import { DialogAddCreateComponent } from './Dialog/dialog-add-create/dialog-add-create.component';
import { DialogDeleteComponent } from './Dialog/dialog-delete/dialog-delete.component';

@NgModule({
  declarations: [
    AppComponent,
    DialogAddCreateComponent,
    DialogDeleteComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MomentDateModule,
    MatSnackBarModule,
    MatIconModule,
    MatDialogModule,
    MatGridListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
