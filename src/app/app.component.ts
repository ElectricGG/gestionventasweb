import {AfterViewInit, Component, ViewChild, OnInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

import {Venta} from './Interfaces/venta';
import { VentaService } from './Services/venta.service';

import {MatDialog} from '@angular/material/dialog';
import { DialogAddCreateComponent } from './Dialog/dialog-add-create/dialog-add-create.component';
import {MatSnackBar} from '@angular/material/snack-bar'
import { DialogDeleteComponent } from './Dialog/dialog-delete/dialog-delete.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit, OnInit{
  displayedColumns: string[] = ['Id', 'Asesor', 'Cliente','Fecha de Venta','Total Venta',"Acciones"];

  dataSource = new MatTableDataSource<Venta>();
  constructor(private _ventaServicio: VentaService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
    ){


  }

  ngOnInit(): void {
    this.mostrarVentas();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }


  mostrarVentas(){
    this._ventaServicio.getListVenta().subscribe({
      next:(dataResponse)=>{
        console.log(dataResponse);
        this.dataSource.data = dataResponse;
      },error:(e)=>{}
    })
  }

  dialogoNuevaVenta(enterAnimationDuration: string, exitAnimationDuration: string): void {
    this.dialog.open(DialogAddCreateComponent, {
      disableClose:true,
      width: '600px',
      enterAnimationDuration,
      exitAnimationDuration,
    }).afterClosed().subscribe(result=>{
      if(result==="creado"){
        this.mostrarVentas();
      }
    });
  }

  mostrarAlerta(msg: string, accion: string) {
    this._snackBar.open(msg, accion, {
      horizontalPosition:"end",
      verticalPosition:"top",
      duration:3000
    });
  }

  dialogoEliminarVenta(dataVenta:Venta){
    
    this.dialog.open(DialogDeleteComponent, {
      disableClose:true,
      data:dataVenta
    }).afterClosed().subscribe(result=>{
      if(result==="eliminar"){
        
        this._ventaServicio.deleteVenta(dataVenta.id).subscribe({
          next:(data)=>{
            this.mostrarAlerta("Venta eliminada","Listo");
            this.mostrarVentas();
          },error:(e)=>{console.log(e)}
        });
      }
    });
  }

}

