import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef,MAT_DIALOG_DATA} from "@angular/material/dialog";

import { Venta } from 'src/app/Interfaces/venta';

@Component({
  selector: 'app-dialog-delete',
  templateUrl: './dialog-delete.component.html',
  styleUrls: ['./dialog-delete.component.css']
})
export class DialogDeleteComponent implements OnInit {

  constructor(
    private dialogoReferencia:MatDialogRef<DialogDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public dataVenta: Venta
  ) { }

  ngOnInit(): void {
  }

  confirmarEliminar(){
    console.log(this.dataVenta);
    if(this.dataVenta){
      console.log(this.dataVenta);
      this.dialogoReferencia.close("eliminar");
    }
  }

}
