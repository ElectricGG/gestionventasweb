import { Component, OnInit } from '@angular/core';

import {FormBuilder,FormGroup,Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MAT_DATE_FORMATS} from "@angular/material/core";
import { VentaService } from 'src/app/Services/venta.service';
import { Asesor } from 'src/app/Interfaces/asesor';
import { Cliente } from 'src/app/Interfaces/cliente';
import { Producto } from 'src/app/Interfaces/producto';
import { AsesorService } from 'src/app/Services/asesor.service';
import { ClienteService } from 'src/app/Services/cliente.service';
import { ProductoService } from 'src/app/Services/producto.service';
import { VentaCreate } from 'src/app/Interfaces/venta-create';
import { VentaDetalleCreate } from 'src/app/Interfaces/venta-detalle-create';
import {MatTableDataSource} from '@angular/material/table';

export const MY_DATE_FORMATS = {
  parse:{
    dateInput: "YYYY/MM/DD",
  },
  display:{
    dateInput:"YYYY/MM/DD",
    monthYearLabel:"MMMM YYYY",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM YYYY"
  }
}


@Component({
  selector: 'app-dialog-add-create',
  templateUrl: './dialog-add-create.component.html',
  styleUrls: ['./dialog-add-create.component.css'],
  providers:[
    {provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS}
  ]
})



export class DialogAddCreateComponent implements OnInit {

  formVenta: FormGroup;
  tituloAccion: string = "Nueva";
  botonAccion: string = "Guardar";
  listaAsesores: Asesor[] = [];
  listaClientes: Cliente[] = [];
  listaProductos: Producto[] = [];
  ventaDetCreate: VentaDetalleCreate[] = [];
  tableAdd:any[] = [];
  displayedColumns: string[] = ['Producto', 'Precio', 'Cantidad'];
  opcionSeleccionada: string="";

  dataSourceDet = new MatTableDataSource<any>();

  constructor(private dialogoReferencia:MatDialogRef<DialogAddCreateComponent>,
              private fbventa: FormBuilder,
              private _snackBar:MatSnackBar,
              private _ventaService:VentaService,
              private _asesorService:AsesorService,
              private _clienteService:ClienteService,
              private _productoService:ProductoService
    ) 
    {  

      this.formVenta = this.fbventa.group({
        asesorId:['',Validators.required],
        clienteId:['',Validators.required],
        fechaVenta:['',Validators.nullValidator],
        productoId:['',Validators.required],
        precioProd:['',Validators.required],
        cantidadProd:['',Validators.required],
      })


      this._asesorService.getListAsesor().subscribe({
        next:(data)=>{
          this.listaAsesores = data;
        },error:(e)=>{}
      });

      this._clienteService.getListCliente().subscribe({
        next:(data)=>{
          this.listaClientes = data;
        },error:(e)=>{}
      });

      this._productoService.getListProducto().subscribe({
        next:(data)=>{
          this.listaProductos = data;
        },error:(e)=>{}
      });
   }

  mostrarAlerta(msg: string, accion: string) {
    this._snackBar.open(msg, accion, {
      horizontalPosition:"end",
      verticalPosition:"top",
      duration:3000
    });
  }

  addDetalle(){
    this.ventaDetCreate.push({
      productoId: this.formVenta.value.productoId,
      precioUnitario: this.formVenta.value.precioProd,
      cantidad: this.formVenta.value.cantidadProd
    });

    this.mostrarDetEnTabla();
  }

  mostrarDetEnTabla(){
    const producto = this.listaProductos.find((producto)=>{
      return producto.id === this.formVenta.value.productoId;
    });

    this.tableAdd.push({
      producto: producto?.nombre,
      precio:this.formVenta.value.precioProd,
      cantidad:this.formVenta.value.cantidadProd
    });
    this.dataSourceDet.data = this.tableAdd;
  }

  addEditVenta(){
    const fecha = new Date();
    const year = fecha.getFullYear();
    const month = (fecha.getMonth() + 1).toString().padStart(2, '0');
    const day = fecha.getDate().toString().padStart(2, '0');
    const fechaFormateada = `${year}-${month}-${day}`;

    this.formVenta.value.fechaVenta = fechaFormateada;
    const venta:VentaCreate={
      id: 0,
      clienteId: this.formVenta.value.clienteId,
      asesorId: this.formVenta.value.asesorId,
      fechaVenta: this.formVenta.value.fechaVenta,
      ventaDetalles: this.ventaDetCreate
    }
    
    this._ventaService.createVenta(venta).subscribe({
      next:(data)=>{
        this.mostrarAlerta("Venta realizada.","Listo");
        this.dialogoReferencia.close("creado");
      },error:(e)=>{
        this.mostrarAlerta("No se pudo crear","Error");
      }
    });

  }

  ngOnInit(): void {
  }

    
}


